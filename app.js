const chk = document.querySelector("#checkbox");
if(window.matchMedia('(prefers-color-scheme: dark)').matches){
    chk.setAttribute('checked',true);
}

chk.addEventListener('change',function(e) {
    document.body.classList.toggle('is-dark-mode');
    if(this.checked){
        document.body.classList.remove('is-light-mode');
        document.body.classList.add('is-dark-mode');
    }else{
        document.body.classList.remove('is-dark-mode');
        document.body.classList.add('is-light-mode');
    }
});
