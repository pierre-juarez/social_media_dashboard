# Social Media Dashboard
_Este si bien es cierto no es un proyecto personal, es un reto que seguí paso a paso en el [canal de Youtube](https://www.youtube.com/LeonidasEsteban) de [Leonidas Esteban](https://leonidasesteban.com/)  (muy cool, por cierto), y en el cual aprendí muchísimo.💪_

Te dejo aquí una demo: https://pierre-juarez.gitlab.io/social_media_dashboard 

## Screenshot Demo:

![screenshot demo](images/screenshot_demo.png?raw=true "Screenshot Demo")

---
⌨️ con ❤️ por [Pierre Juarez](https://gitlab.com/pierre-juarez) 😊
